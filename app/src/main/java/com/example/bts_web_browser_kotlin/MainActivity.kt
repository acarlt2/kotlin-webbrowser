package com.example.bts_web_browser_kotlin

import android.app.Activity
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    lateinit var webview:WebView
    lateinit var searchButton:ImageButton
    lateinit var backButton:ImageButton
    lateinit var urlText:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webview = findViewById(R.id.webview)
        urlText = findViewById(R.id.url)
        backButton = findViewById(R.id.back_btn)
        searchButton = findViewById(R.id.search_btn)

        backButton.setOnClickListener {
            if (webview != null && webview.canGoBack())
                webview.goBack();
            else
                webview.loadUrl("https://www.google.com");
        }


        searchButton.setOnClickListener {
            var utext = urlText.getText().toString();
            if (urlText != null)

                webview.loadUrl(utext);
            else
                webview.loadUrl("https://www.google.com");
        }




        webview.loadUrl("https://www.google.com")
        webview.settings.javaScriptEnabled = true // we need to enable javascript
        webview.canGoBack()
        webview.webViewClient = WebClient(this)


    }



    class WebClient internal constructor(private val activity: Activity): WebViewClient(){
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }
}